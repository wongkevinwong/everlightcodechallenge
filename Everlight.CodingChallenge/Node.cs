﻿namespace Everlight.CodingChallenge
{
    public class Node
    {
        public int Id { get; set; }

        public Node ParentNode { get; set; }
        public int Level { get; set; }

    }
}
