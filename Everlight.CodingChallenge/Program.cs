﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Everlight.CodingChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var trees = BuildTree();

            Console.WriteLine("Press <enter> to kick start");

            while (Console.ReadLine() != null)
            {
                Console.WriteLine("Please enter number of ball : ");
                int numberOfBall = int.Parse(Console.ReadLine());

                var resultForEachBall = new List<KeyValuePair<int, IList<Node>>>();
                var maxLevel = trees.Max(x => x.Level);
                for (int x = 0; x < numberOfBall; x++)
                {
                    var result = new List<Node>();
                    var nextNode = trees.FirstOrDefault(b => b.Level == 0);
                    for (int i = 0; i < maxLevel; i++)
                    {
                        var node = nextNode;
                        result.Add(node);
                        nextNode = new Program().NextChild(trees, node);

                        if (i == maxLevel - 1)
                            result.Add(nextNode);
                    }
                    resultForEachBall.Add(new KeyValuePair<int, IList<Node>>(x + 1, result));
                }

                var nodesOfEachBalls = resultForEachBall.Select(x => x.Value);

                var lastNodeOfEachBalls = trees.Where(x => x.Level == maxLevel).Select(x => Map(x.Id)).Except(nodesOfEachBalls.Select(x => Map(x[x.Count - 1].Id)));

                Console.WriteLine(string.Concat("Containers that doesn't receive a ball when all balls have passed through the system are : ", string.Join(", ", lastNodeOfEachBalls)));

                Console.WriteLine("Press <enter> to repeat or press CTRL+C to exit");
            }
            Console.ReadLine();
        }

        private static IList<Node> BuildTree()
        {
            var tree = new List<Node>();

            var node = new Node();
            node.Id = 1;
            node.Level = 0;
            node.ParentNode = null;
            tree.Add(node);

            node = new Node();
            node.Id = 2;
            node.Level = 1;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 1);
            tree.Add(node);

            node = new Node();
            node.Id = 3;
            node.Level = 1;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 1);
            tree.Add(node);


            node = new Node();
            node.Id = 4;
            node.Level = 2;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 2);
            tree.Add(node);

            node = new Node();
            node.Id = 5;
            node.Level = 2;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 2);
            tree.Add(node);


            node = new Node();
            node.Id = 6;
            node.Level = 2;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 3);
            tree.Add(node);

            node = new Node();
            node.Id = 7;
            node.Level = 2;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 3);
            tree.Add(node);




            node = new Node();
            node.Id = 8;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 4);
            tree.Add(node);


            node = new Node();
            node.Id = 9;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 4);
            tree.Add(node);


            node = new Node();
            node.Id = 10;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 5);
            tree.Add(node);


            node = new Node();
            node.Id = 11;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 5);
            tree.Add(node);


            node = new Node();
            node.Id = 12;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 6);
            tree.Add(node);


            node = new Node();
            node.Id = 13;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 6);
            tree.Add(node);


            node = new Node();
            node.Id = 14;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 7);
            tree.Add(node);


            node = new Node();
            node.Id = 15;
            node.Level = 3;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 7);
            tree.Add(node);






            node = new Node();
            node.Id = 16;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 8);
            tree.Add(node);


            node = new Node();
            node.Id = 17;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 8);
            tree.Add(node);

            node = new Node();
            node.Id = 18;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 9);
            tree.Add(node);

            node = new Node();
            node.Id = 19;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 9);
            tree.Add(node);

            node = new Node();
            node.Id = 20;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 10);
            tree.Add(node);

            node = new Node();
            node.Id = 21;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 10);
            tree.Add(node);

            node = new Node();
            node.Id = 22;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 11);
            tree.Add(node);

            node = new Node();
            node.Id = 23;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 11);
            tree.Add(node);

            node = new Node();
            node.Id = 24;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 12);
            tree.Add(node);

            node = new Node();
            node.Id = 25;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 12);
            tree.Add(node);

            node = new Node();
            node.Id = 26;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 13);
            tree.Add(node);

            node = new Node();
            node.Id = 27;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 13);
            tree.Add(node);

            node = new Node();
            node.Id = 28;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 14);
            tree.Add(node);

            node = new Node();
            node.Id = 29;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 14);
            tree.Add(node);


            node = new Node();
            node.Id = 30;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 15);
            tree.Add(node);


            node = new Node();
            node.Id = 31;
            node.Level = 4;
            node.ParentNode = tree.FirstOrDefault(x => x.Id == 15);
            tree.Add(node);

            return tree;
        }


        public Node NextChild(IList<Node> tree, Node currentNode)
        {
            byte[] randomNumber = new byte[1];
            RNGCryptoServiceProvider Gen = new RNGCryptoServiceProvider();
            Gen.GetBytes(randomNumber);
            int rand = Convert.ToInt32(randomNumber[0]);
            var firstOrSecondChild = rand % 2 + 1;

            var children = tree.Where(x => x.Level == currentNode.Level + 1 && x.ParentNode == currentNode);
            Node selectedChild = null;
            if (firstOrSecondChild == 1)
                selectedChild = children.OrderBy(x => x.Id).FirstOrDefault();
            else
                selectedChild = children.OrderByDescending(x => x.Id).FirstOrDefault();

            return selectedChild;
        }

        public static string Map(int id)
        {
            string mapResult = "";
            switch (id)
            {
                case 16:
                    mapResult = "A";
                    break;
                case 17:
                    mapResult = "B";
                    break;
                case 18:
                    mapResult = "C";
                    break;
                case 19:
                    mapResult = "D";
                    break;
                case 20:
                    mapResult = "E";
                    break;
                case 21:
                    mapResult = "F";
                    break;
                case 22:
                    mapResult = "G";
                    break;
                case 23:
                    mapResult = "H";
                    break;
                case 24:
                    mapResult = "I";
                    break;
                case 25:
                    mapResult = "J";
                    break;
                case 26:
                    mapResult = "K";
                    break;
                case 27:
                    mapResult = "L";
                    break;
                case 28:
                    mapResult = "M";
                    break;
                case 29:
                    mapResult = "N";
                    break;
                case 30:
                    mapResult = "O";
                    break;
                case 31:
                    mapResult = "P";
                    break;
                default:
                    break;
            }

            return mapResult;
        }
    }
}
